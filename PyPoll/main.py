#!/usr/bin/env python
# coding: utf-8

import csv
f = 'election_data.csv'

with open("election_data.csv") as f:
    reader = csv.reader(f)
    next(reader)
    data = [r for r in reader]

f.close()
# We only need the third column for all the calcs
votes = [i[2] for i in data]
votes_set = set(votes)
unique_candidates = list(votes_set)
total_votes = len(votes)
vote_counts = {i:votes.count(i) for i in unique_candidates}
sorted_by_votes = sorted(vote_counts, key=vote_counts.__getitem__, reverse=True)

print("Election Results")
print("-----------------")
print(f"Total Votes: {total_votes}")
print("-----------------")
for k in sorted_by_votes:
    percentage = vote_counts[k] / total_votes
    print(f"{k}" +  ": " + "{:5.2%}".format(percentage) + ": " + f"({vote_counts[k]})")
print("-----------------")
print(f"Winner: {sorted_by_votes[0]}")

# {voterid:12864552, county: Marsh, Candidate: Khan}
#voter_id[]
# output to file 
fo = open('py_poll.txt', 'w')
fo.write("Election Results\n")
fo.write("-----------------------\n")
fo.write(f"Total Votes: {total_votes}\n")
fo.write("-----------------\n")
for k in sorted_by_votes:
    percentage = vote_counts[k] / total_votes
    fo.write(f"{k}" +  ": " + "{:5.2%}".format(percentage) + ": " + f"({vote_counts[k]})\n")
fo.write("-----------------\n")
fo.write(f"Winner: {sorted_by_votes[0]}\n")
fo.close()