#!/usr/bin/env python

import csv
# import locale for U.S. currency formatting
import locale
locale.setlocale(locale.LC_ALL, '')

months = []
monthly_dollars = []
monthly_delta = []
data = []
deltas = []
avg_delta = 0
max_delta = 0
min_delta = 0
record_count = 0
total_spend = 0
max_delta_month = ""
min_delta_month = ""


# get list of the delta of budget between month and next month
def gen_monthly_budget_delta(l_dollars):
    # start at index 1 (second item in list of dollar spend) because first month has no delta calc
    l_deltas = [l_dollars[i] - l_dollars[i-1] for i in range(1,len(l_dollars))]
    return l_deltas

def get_max_delta(l_dollars):
    return max(l_dollars)

def get_min_delta(l_dollars):
    return min(l_dollars)

def gen_avg_delta(l_dollars):
    l_avg_dollars = sum(l_dollars) / len(l_dollars)
    return l_avg_dollars

def sum_dollars(l_dollars):
    return sum(l_dollars)

def get_record_count(months_records):
    return len(months_records)

def find_max_delta_month(month_records, delta_list):
    # Find the month with max delta
    # return the month based on the index of the dollars
    # Note: because there is no delta for the 'Zeroth' index (first month in records)
    # need to add 1 to index and return that
     return month_records[delta_list.index(max(delta_list)) +1]

def find_min_delta_month(month_records, delta_list):
    # Find the month with min delta
    # return the month based on the index of the dollars
    # Note: because there is no delta for the 'Zeroth' index (first month in records)
    # need to add 1 to index and return that
    return month_records[delta_list.index(min(delta_list)) +1]   

def format_us_dollars(money):
    return locale.currency(money, grouping=True)

######################################


with open("budget_data.csv") as f:
    reader = csv.reader(f)
    # skip header
    next(reader)
    data = [r for r in reader]
f.close()

months = [i[0] for i in data]
monthly_dollars = [int(i[1]) for i in data]

deltas = gen_monthly_budget_delta(monthly_dollars)
total_spend = sum_dollars(monthly_dollars)
max_delta = get_max_delta(deltas)
min_delta = get_min_delta(deltas)
avg_delta = gen_avg_delta(deltas)
record_count = get_record_count(months)
max_delta_month = find_max_delta_month(months,deltas)
min_delta_month = find_min_delta_month(months,deltas)

print(f"Total Months: {record_count}")
print(f"Avg change: {format_us_dollars(avg_delta)}")
print(f"Total: {format_us_dollars(total_spend)}")
print(f"Greatest Increase in Profits: {max_delta_month} {format_us_dollars(max_delta)}")
print(f"Greatest Decrease in Profits: {min_delta_month} ({format_us_dollars(min_delta)} ")

# output to file 
fo = open('py_bank.txt', 'w')
fo.write("Financial Analysis\n")
fo.write("-----------------------\n")
fo.write(f"Total Months: {record_count}\n")
fo.write(f"Total: {format_us_dollars(total_spend)}\n")
fo.write(f"Avg change: {format_us_dollars(avg_delta)}\n")
fo.write(f"Greatest Increase in Profits: {max_delta_month} ({format_us_dollars(max_delta)})\n")
fo.write(f"Greatest Decrease in Profits: {min_delta_month} ({format_us_dollars(min_delta)})\n")
fo.close()
#
